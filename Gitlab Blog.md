
## New Feature, No worries  

<br>

### A simple guide to adding new feaures via branch and merge request using Gitlabs Web EDI  [Gitlab Web Edi](https://gitlab.com/gitlab-org/gitlab-web-edi)  

<br>
<br>

### You are excited! You see a solution and way to build out that feature in your head. You know the steps to take to make it work, and how amazing it will be once implemented. You will be viewed as a hero, labeled as a visionary, and hailed for your technical prowess. The only issue is…. You don’t want to break what you have already created to this point.   

<br>

### This is a valid concern and one experienced often by readers of this *Well Known Blog*. 

<br>

### That is why I am going to show you step by step a simple way to lay the groundwork for getting started on that new feature. 

<br>

### This tutorial will also be of great use to students working on Google Chrome machines, developers working on public machines, and developers who are looking to make quick feature edits.

<br>

### If you haven't already, head over and create a free account on [Gitlab](https://about.gitlab.com/free-trial) where you can enjoy a thirty day free trial without putting down a credit card as the free tier is always free.  

<br>
<br>
<br>


1. Cool, Gitlab account created, the next thing we need to do is clone a copy of the repo to your local machine right? No, no you do not actually!  

<br>

2. Access the Project from your Gitlab dashboard. 
For this project i used the Gatsby Template provided by Gitlab.  

<br>

![Gatsby Template](/assets/images/template.png)

<br>

3. Select “Create New Branch”  

<br>  

![Gatsby Template](/assets/images/branchcreate.png)  

<br>

4. Go ahead and name and create the branch as shown below  

<br>

![Gatsby Template](/assets/images/newbranch.png)  

<br>

5. Now back on the dashboard you will notice the branch has automatically changed to our newley created. Thanks Gitlab!  

<br>  

![Gatsby Template](/assets/images/dashdemo.png)  

<br>

5. Clone to a local machine? No, but thanks for asking!!  

<br>  

![Gatsby Template](/assets/images/webedi.png)

<br>

6. Open the new Web EDI from the dashboard. We can make edits and changes right from the edi the same as we would on our local machines!  

<br>

![Gatsby Template](/assets/images/edishot.png) 

<br>

7. Commit changes  

<br>

![Gatsby Template](/assets/images/commit.png)  

<br>

8. Merge  

<br>  

![Gatsby Template](/assets/images/mergescreen.png)

<br>

9. Nice Job!!!  

<br> 

![Gatsby Template](/assets/images/done.png)  

<br>

10. Drink some water, walk the dog, party like a rockstar and celebrate your new feature!  

<br>

![Gatsby Template](/assets/images/berniebear.jpg)  
  



